/** @type {import('tailwindcss').Config} */
module.exports = {
  purge: ['layouts/**/*.html'],
  content: [
    './layouts/**/*.html'
  ],
  darkMode: 'media',
  theme: {
    extend: {
      colors: {
        'apple-white': '#fafafc',
        'apple-black': '#1d1d1f',
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
}