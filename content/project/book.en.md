+++
title = "Book specs"
type = "project"
coverPhoto = "http://www.thomasbruggisser.ch/abbildungen/super-welcome-to-graphic-wonderland/_gross/Super-13.jpg"
izvajalec = "Ivana banovič, Simon Levar"
tipNaloge = "Analogue"
weight = 1
+++

## Super Welcome to Graphic Wonderland
Thomas Bruggisser, Michael Fries  
Zurich, Switzerland  

![test](http://www.thomasbruggisser.ch/abbildungen/super-welcome-to-graphic-wonderland/_gross/Super-13.jpg)

Publisher: *Die Gestalten Verlag GmbH, Berlin, Germany, 2003*  
ISBN: 9783899550054


---

### Dust Jacket

Dimensions: Deep pins - 124 mm  
Cardboard dust jacket with a double flap  
4-fold scored  
Paper: Bindakote 250g (one sided, ice white) (...or similar – WFC satin),  
Offset print 2/0 - Fluo yellow + Black (roughly)  
Additional processing - cut out the size of 15x15 cm (text form) - vector attached in the file jacket_cutout.pdf  

---

### Insert  
Insert - 490 mm x 720 mm, vertical format  
Paper: Newspaper paper  
Print: 1/1 - Black  
Folded to 125 mm x 180 mm (folded 4 times)  

---

### Book Block
336 pages  
165 mm x 240 mm, portrait  
Open stitched with white thread  
Spine covered with transparent glue  

**1. Booklet**  
16 pages   
Paper: Arctic Volume White 130g - the back side is rougher, thin WoodFree coating    
Print: Offset 1/1 – Process Black  

**2. – 5. Booklet**  
8 pages  
Paper: IQ color neon 80g - neon yellow   
Print: Offset 1/1 – Process Black  

**6. Booklet**  
16 + 4 pages   
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

Wrapped in a sheet (first and last two pages) – SH Recycling grey/grey(52) 140g  
Print: unprinted (0/0)  

**7. – 8. Booklet**  
16 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

**Inserted sheet**  
2 pages  
SH Recycling 140g grey/grey(52)   
Print: unprinted (0/0)   

**9. – 10. Booklet**  
8 pages  
Paper: IQ color neon 80g - neon yellow  
Print: Offset 4/4 – CMYK  

**Inserted sheet**  
2 pages  
SH Recycling 140g grey/grey(52)  
Print: unprinted (0/0)  

**11. Booklet**  
16 + 4 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

Wrapped in a sheet (first two and last two pages of the booklet) – SH Recycling grey/grey(52) 140g  
Print: unprinted (0/0)  

**12. Booklet**  
16 pages  
SH Recycling 140g grey/grey(52)   
Print: Offset 4/4 :
- Process Black
- Process Blue
- Silver metallic
- Lacquer

**13. Booklet**  
16 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

**14. Booklet**  
16 pages  
SH Recycling 140g grey/grey(52)  
Print: Offset 4/4 :
- Process Black
- Process Blue
- Silver metallic
- Lacquer

**15. Booklet**  
16 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

**16. Booklet**  
16 + 4 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

Wrapped in a sheet (first and last two pages) – SH Recycling grey/grey(52) 140g  
Print: unprinted (0/0)  

**17. Booklet**  
16 pages  
SH Recycling 140g grey/grey(52)  
Print: Offset 4/4 :
- Process Black
- Process Blue
- Silver metallic
- Lacquer

**18. Booklet**  
16 + 4 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

Wrapped in a sheet (first and last two pages) – SH Recycling grey/grey(52) 140g  
Print: unprinted (0/0)  

**19. Booklet**  
16 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

**20. Booklet**  
16 + 4 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

Wrapped in a sheet (first and last two pages) – SH Recycling grey/grey(52) 140g  
Print: unprinted (0/0)  

**21. Booklet**  
16 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

**22. Booklet**  
16 pages  
Arctic Volume White 130g - the back side is rougher, thin WoodFree coating  
Folded – smooth against smooth, the first page is smooth  
Print: Offset 4/4 – CMYK  

**23. Booklet**  
16 + 4 pages  
Paper: IQ color neon 80g - neon yellow   
Print: Offset 4/4 – CMYK  

Wrapped in a sheet (first and last two pages) – SH Recycling grey/grey(52) 140g  
Print: unprinted (0/0)  
