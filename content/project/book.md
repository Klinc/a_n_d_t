+++
title = "Specifikacija knjige"
type = "project"
coverPhoto = "http://www.thomasbruggisser.ch/abbildungen/super-welcome-to-graphic-wonderland/_gross/Super-13.jpg"
izvajalec = "Ivana banovič, Simon Levar"
tipNaloge = "Analogno"
weight=1
+++

## Super Welcome to Graphic Wonderland
Thomas Bruggisser, Michael Fries  
Zurich, Switzerland

![test](http://www.thomasbruggisser.ch/abbildungen/super-welcome-to-graphic-wonderland/_gross/Super-13.jpg)

Založba: *Die Gestalten Verlag GmbH, Berlin, Germany, 2003*  
ISBN: 9783899550054

---

### Zaščitni ovitek

Dimenzije: Globoki zatiči – 124 mm  
Kartonski zaščitni ovitek z dvojnim zavihkom  
4-krat bigano  
Papir: Bindakote 250g (one sided, ice white) (...ali podobna – WFC satin),  
Offset tisk 2/0 – Fluo rumena + Črna (na grobo)  
Dodatna obdelava – izzsek v velikosti 15x15 cm (tekstovna oblika) – vektor priložen v datoteki ovitek_izrez.pdf  

---

### Vložek
Vložek – 490 mm x 720 mm, pokončno format
Papir: Časopisni papir
Tisk: 1/1 – Črna
Preopgnjeno na 125 mm x 180 mm (prepognjeno 4 - krat)

---

### Knjižni blok
336 strani
165 mm x 240 mm, portret
Odprto šivan z belim sukancem
Hrbet prekrit s transparentnim lepilom

**1. Zvezek**  
16 strani  
Papir : Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Tisk: Offset 1/1 – Process Črna  

**2. – 5. Zvezek**  
8 strani  
Papir: IQ color neon 80g – neonsko rumena   
Tisk: Offset 1/1 – Process Črna  

**6. Zvezek**  
16 + 4 strani   
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

Ovito v list (prvi in zadnji dve strani) – SH Recycling grey/grey(52) 140g  
Tisk: nepotiskan (0/0)  

**7. – 8. zvezek**  
16 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

**Vstavljen list**  
2 strani  
SH Recycling 140g grey/grey(52)   
Tisk: nepotiskan (0/0)   

**9. – 10. Zvezek**  
8 strani  
Papir: IQ color neon 80g – neonsko rumena  
Tisk: Offset 4/4 – CMYK  

**Vstavljen list**  
2 strani  
SH Recycling 140g grey/grey(52)  
Tisk: nepotiskan (0/0)  

**11. Zvezek**  
16 + 4 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

Ovito v list (prvi dve in zadnji dve strani zvezka) – SH Recycling grey/grey(52) 140g  
Tisk: nepotiskan (0/0)  

**12. Zvezek**  
16 strani  
SH Recycling 140g grey/grey(52)   
Tisk: Offset 4/4 :
- Process Black
- Process Blue
- Srebrna metalic
- Lak

**13. Zvezek**  
16 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

**14. Zvezek**  
16 strani  
SH Recycling 140g grey/grey(52)  
Tisk: Offset 4/4 :
- Process Black
- Process Blue
- Srebrna metalic
- Lak

**15. Zvezek**  
16 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

**16. Zvezek**  
16 + 4 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

Ovito v list (prvi in zadnji dve strani) – SH Recycling grey/grey(52) 140g  
Tisk: nepotiskan (0/0)  

**17. Zvezek**  
16 strani  
SH Recycling 140g grey/grey(52)  
Tisk: Offset 4/4 :
- Process Black
- Process Blue
- Srebrna metalic
- Lak

**18. Zvezek**  
16 + 4 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

Ovito v list (prvi in zadnji dve strani) – SH Recycling grey/grey(52) 140g  
Tisk: nepotiskan (0/0)  

**19. Zvezek**  
16 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

**20. Zvezek**  
16 + 4 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

Ovito v list (prvi in zadnji dve strani) – SH Recycling grey/grey(52) 140g  
Tisk: nepotiskan (0/0)  


**21. Zvezek**  
16 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

**22. Zvezek**  
16 strani  
Arctic Volume White 130g – zadnja stran bolj groba, tanek WoodFree premaz  
Zlaganje – gladka nasproti gladki, prva stran je gladka  
Tisk: Offset 4/4 – CMYK  

**23. Zvezek**  
16 + 4 strani  
Papir: IQ color neon 80g – neonsko rumena   
Tisk: Offset 4/4 – CMYK  

Ovito v list (prvi in zadnji dve strani) – SH Recycling grey/grey(52) 140g  
Tisk: nepotiskan (0/0)

---
