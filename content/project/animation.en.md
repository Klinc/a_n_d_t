+++
title = "Animation"
type = "project"
coverPhoto = "/apx_poster_large.jpeg"
izvajalec = "Luka Umek"
tipNaloge = "Digital"
weight = 4
+++
## Base
{{< youtube 2cL9WsT_KFU >}}
![](/apx_poster_large.jpeg)


## Mockup
{{< youtube ru648vxHwOc >}}
![](/animation1.jpeg)