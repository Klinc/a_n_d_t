+++
title = "Priprava za tisk in spletno objavo"
type = "project"
coverPhoto = "/print/ranger.jpg"
izvajalec = "Dorija Šiško"
tipNaloge = "Iz analognega v digitalno"
weight = 3
+++
[↓ Prenesi vse - Drive](https://drive.google.com/file/d/1oEQ6-TFuNbqNEZYWX8QEPCf-PtOpmGOs/view?usp=sharing)
## Tiskovine


### Pantone
![Dvobarvni Pantone](/print/pantone.jpg)
PMS BLUE 072 C + PMS 7729 C
### Sitotisk
![Sitotisk](/print/ranger.jpg)
### Risotisk
![Risotisk](/print/RISO.jpg)

---

## Spletne vsebine

![barvna ikona](/print/ikonca_barvna.svg)

![čb ikona](/print/barvna_skala.jpg)

![profile mockup](/print/profile_mockup.jpg)

![event cover](/print/event_cover.jpg)

![ig mockup](/print/ig_mockup.jpg)