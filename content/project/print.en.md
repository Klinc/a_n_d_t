+++
title = "Prep for print and web publishing"
type = "project"
coverPhoto = "/print/ranger.jpg"
izvajalec = "Dorija Šiško"
tipNaloge = "Analogue to digital"
weight = 3
+++
[↓ Download all - Drive](https://drive.google.com/file/d/1oEQ6-TFuNbqNEZYWX8QEPCf-PtOpmGOs/view?usp=sharing)
## Prints


### Pantone
![Dvobarvni Pantone](/print/pantone.jpg)
PMS BLUE 072 C + PMS 7729 C
### Screen print
![Sitotisk](/print/ranger.jpg)
### Riso print
![Risotisk](/print/RISO.jpg)

---

## Web content

![barvna ikona](/print/ikonca_barvna.svg)

![čb ikona](/print/barvna_skala.jpg)

![profile mockup](/print/profile_mockup.jpg)

![event cover](/print/event_cover.jpg)

![ig mockup](/print/ig_mockup.jpg)